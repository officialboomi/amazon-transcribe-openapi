# Amazon Transcribe Connector
Operations and objects for transcribing speech to text.

Documentation: https://docs.aws.amazon.com/transcribe

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/transcribe/2017-10-26/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

